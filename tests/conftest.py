import pytest
import os
from appium import webdriver

PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)


@pytest.fixture()
def setup(request):
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    #desired_caps['platformVersion'] = '8.1'
    desired_caps['platformVersion'] = '9'
    #desired_caps['deviceName'] = 'Pixel'
    desired_caps['deviceName'] = 'Galaxy A10'
    # Returns abs path relative to this file and not cwd
    # desired_caps['app'] = os.path.abspath(os.path.join(os.path.dirname(__file__),'1.1 ApiDemos-debug.apk.apk'))
    desired_caps['appPackage'] = 'com.dgmarket.android'
    desired_caps['appActivity'] = 'com.dgmarket.android.ui.activity.SplashActivity'
    desired_caps['noReset'] = 'true'
    desired_caps['unicodeKeyboard'] = 'true'

    url = 'http://localhost:4723/wd/hub'
    request.instance.driver = webdriver.Remote(url, desired_caps)

    # def teardown():
    #     request.instance.driver.quit()
    # request.addfinalizer(teardown)

@pytest.fixture()
def SetUp(request):
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    #desired_caps['platformVersion'] = '8.1'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'Galaxy A10'
    #desired_caps['deviceName'] = 'Pixel'
    # Returns abs path relative to this file and not cwd
    # desired_caps['app'] = os.path.abspath(os.path.join(os.path.dirname(__file__),'1.1 ApiDemos-debug.apk.apk'))
    desired_caps['appPackage'] = 'com.dgmarket.android'
    desired_caps['appActivity'] = 'com.dgmarket.android.ui.activity.SplashActivity'
    desired_caps['unicodeKeyboard']='true'
    #desired_caps['noReset'] = 'true'

    url = 'http://localhost:4723/wd/hub'
    request.instance.driver = webdriver.Remote(url, desired_caps)

    def teardown():
        request.instance.driver.quit()
    request.addfinalizer(teardown)