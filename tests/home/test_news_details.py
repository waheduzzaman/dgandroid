import pytest
from test_page.home.news_details_page import NewsDetailsPage
from utilities.teststatus import  TestStatus
import unittest

@pytest.mark.usefixtures('setup')
class NewsDetailsTest(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def classSetup(self,setup):
        self.newsobject = NewsDetailsPage(self.driver)
        self.teststatus= TestStatus(self.driver)

    @pytest.mark.run(order=1)
    def test_NewsDetails(self):
        self.newsobject.NewsDetails()
        result2=self.newsobject.VerifyNewsDetails()
        self.teststatus.markFinal("test_NewsDetails",result2,"news details viewed")