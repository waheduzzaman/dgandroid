import unittest
import pytest
from test_page.home.notification_page import Notifications
from utilities.teststatus import  TestStatus


@pytest.mark.usefixtures('setup')
class NotificationTest(unittest.TestCase):
    @pytest.fixture(autouse=True)
    def classSetup(self,setup):
        self.notiobject = Notifications(self.driver)
        self.teststatus = TestStatus(self.driver)

    @pytest.mark.run(order=1)
    def test_notification(self):
        self.notiobject.NotificationView()
        result1 = self.notiobject.VerifyNotiView()
        self.teststatus.markFinal("test_notification", result1, "Notifications viewed successfully")