import pytest
from test_page.home.news_search_page import NewsSearchPage
from utilities.teststatus import  TestStatus
import unittest

@pytest.mark.usefixtures('setup')
class NewsSearchTest(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def classSetup(self,setup):
        self.newsobject = NewsSearchPage(self.driver)
        self.teststatus= TestStatus(self.driver)

    @pytest.mark.run(order=1)
    def test_simpleNewsSearch(self):
        self.newsobject.SimpleSearch("Educational")
        result2=self.newsobject.verifySimpleSearch()
        self.teststatus.markFinal("test_simpleNewsSearch",result2,"news search successful")

    @pytest.mark.run(order=2)
    def test_searchNoResult(self):
        self.newsobject.SearchNoResult(".,")
        result2 = self.newsobject.verifySearchNoResult()
        self.teststatus.markFinal("test_searchNoResult", result2, "no result found")

    @pytest.mark.run(order=3)
    def test_searchMultipleSectors(self):
        self.newsobject.SearchMultipleSectors()
        result2 = self.newsobject.verifySearchMultipleSecrtors()
        self.teststatus.markFinal("test_searchMultipleSectors", result2, "search result found")

    @pytest.mark.run(order=4)
    def test_SearchMultipleCountries(self):
        self.newsobject.SearchMultipleCountries()
        result2 = self.newsobject.verifySearchMultipleCOuntries()
        self.teststatus.markFinal("test_SearchMultipleCountries", result2, "search result found")

    @pytest.mark.run(order=5)
    def test_SearchFundingAgency(self):
        self.newsobject.SearchFundingAgency()
        result2 = self.newsobject.VerifyFundingSearch()
        self.teststatus.markFinal("test_SearchFundingAgency", result2, "search result found")