import pytest
from test_page.home.forgot_password_page import ForgotPasswordPage
from utilities.teststatus import  TestStatus
import unittest

@pytest.mark.usefixtures('SetUp')
class ForgotPasswordTest(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def classSetup(self,SetUp):
        self.forgotobject = ForgotPasswordPage(self.driver)
        self.teststatus= TestStatus(self.driver)

    @pytest.mark.run(order=1)
    def test_forget_pass_form(self):
        self.forgotobject.ForgetPasswordform()
        result2=self.forgotobject.verifyFOrgotPassFormShown()
        self.teststatus.markFinal("test_forget_pass_form",result2,"forgot password form found")

    @pytest.mark.run(order=2)
    def test_forget_pass_success(self):
        self.forgotobject.ForgetPassInitiate("pdbd02@gmail.com")
        result2 = self.forgotobject.verifyForgetPass()
        self.teststatus.markFinal("test_forget_pass_success", result2, "forgot password done successfully")

    @pytest.mark.run(order=3)
    def test_forgot_pass_InvalidEmail(self):
        self.forgotobject.InvalidEmail("asdasd")
        result2 = self.forgotobject.verifyInvalidEmail()
        self.teststatus.markFinal("test_forgot_pass_InvalidEmail", result2, "system recognized invalid email")

    @pytest.mark.run(order=4)
    def test_forgot_pass_EmptyFields(self):
        self.forgotobject.TryWithEmptyFields()
        result2 = self.forgotobject.VerifyEmptyFieldTry()
        self.teststatus.markFinal("test_forgot_pass_EmptyFields", result2, "Cannot submit empty forget password form")









