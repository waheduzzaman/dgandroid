import pytest
from test_page.home.notice_search_page import NoticeSearchPage
from utilities.teststatus import  TestStatus
import unittest

@pytest.mark.usefixtures('setup')
class NoticeSearchTest(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def classSetup(self,setup):
        self.searchobject = NoticeSearchPage(self.driver)
        self.teststatus= TestStatus(self.driver)

    # @pytest.mark.run(order=1)
    # def test_simpleNoticeSearch(self):
    #     self.searchobject.SimpleSearch("construction supervision","construction work")
    #     result2=self.searchobject.verifySimpleSearch()
    #     self.teststatus.markFinal("test_simpleNoticeSearch",result2,"notice search successful")
    #
    # @pytest.mark.run(order=2)
    # def test_simpleNoticeSearch2(self):
    #     self.searchobject.SimpleSearch2("road")
    #     result2 = self.searchobject.verifySimpleSearch2()
    #     self.teststatus.markFinal("test_simpleNoticeSearch2", result2, "notice search successful")
    #
    # @pytest.mark.run(order=3)
    # def test_SearchNoResult(self):
    #     self.searchobject.SearchNoResult("!!!")
    #     result2 = self.searchobject.verifySearchNoResult()
    #     self.teststatus.markFinal("test_SearchNoResult", result2, "notice search successful")
    #
    # @pytest.mark.run(order=4)
    # def test_SearchEstimatedValue(self):
    #     self.searchobject.SearchEstimatedVAlue("1000","5000")
    #     result2 = self.searchobject.verifyEstimatedValueSearch()
    #     self.teststatus.markFinal("test_SearchEstimatedValue", result2, "notice search successful")
    #
    # @pytest.mark.run(order=5)
    # def test_SearchPublishedDate(self):
    #     self.searchobject.PublishedDateSearch()
    #     result2 = self.searchobject.VerifyPublishSearch()
    #     self.teststatus.markFinal("test_SearchPublishedDate", result2, "notice search successful")
    #
    # @pytest.mark.run(order=6)
    # def test_PublishedEndDateSearch(self):
    #     self.searchobject.PublishedEndDateSearch()
    #     result2 = self.searchobject.VerifyEndPublishDate()
    #     self.teststatus.markFinal("test_PublishedEndDateSearch", result2, "notice search successful")
    #
    @pytest.mark.run(order=7)
    def test_NoticeDetailsViewAfterSearch(self):
        self.searchobject.NoticeDetailsViewAfterSearch("construction supervision","construction work")
        result2 = self.searchobject.VerifyNoticeDetailsViewAfterSearch()
        self.teststatus.markFinal("test_NoticeDetailsViewAfterSearch", result2, "notice details viewed after search")

    @pytest.mark.run(order=8)
    def test_SearchWIthBuyerName(self):
        self.searchobject.SearchWIthBuyerName("world bank")
        result2 = self.searchobject.VerifySearchBuyerName()
        self.teststatus.markFinal("test_SearchWIthBuyerName", result2, "notice found with buyer name")



