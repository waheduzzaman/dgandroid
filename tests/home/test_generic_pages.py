import unittest
import pytest
from test_page.home.generic_pages import GenericPages
from utilities.teststatus import  TestStatus


@pytest.mark.usefixtures('setup')
class GenericPagesTest(unittest.TestCase):
    @pytest.fixture(autouse=True)
    def classSetup(self,setup):
        self.genericobject = GenericPages(self.driver)
        self.teststatus = TestStatus(self.driver)

    @pytest.mark.run(order=1)
    def test_FeedbackSubmission(self):
        self.genericobject.SendFeedbackTrue("Test","Test")
        result1 = self.genericobject.verifyFeedbackSubmission()
        self.teststatus.markFinal("test_FeedbackSubmission", result1, "feedback submitted successfully")

    @pytest.mark.run(order=2)
    def test_FeedbackSubmissionNoSub(self):
        self.genericobject.SendFeedbackWithoutSubject("Test")
        result1 = self.genericobject.VerifyFeebackWithoutSub()
        self.teststatus.markFinal("test_FeedbackSubmissionNoSub", result1, "no subject message shown")

    @pytest.mark.run(order=3)
    def test_FeedbackSubmissionNoMsg(self):
        self.genericobject.SendFeedbackWIthoutmsg("Test")
        result1 = self.genericobject.VerifyFeedbackWithoutMsg()
        self.teststatus.markFinal("test_FeedbackSubmissionNoMsg", result1, "no message error shown")

    @pytest.mark.run(order=4)
    def test_AboutUs(self):
        self.genericobject.AboutUs()
        result1 = self.genericobject.VerifyAboutUs()
        self.teststatus.markFinal("test_AboutUs", result1, "About us page viewed")

    @pytest.mark.run(order=5)
    def test_RateUs(self):
        self.genericobject.RateUs()
        result1 = self.genericobject.VerifyRateUs()
        self.teststatus.markFinal("test_RateUs", result1, "Rate us page viewed")

    @pytest.mark.run(order=6)
    def test_Terms(self):
        self.genericobject.TermsConditions()
        result1 = self.genericobject.VerifyTerms()
        self.teststatus.markFinal("test_Terms", result1, "Terms and conditions page viewed")

    @pytest.mark.run(order=7)
    def test_PrivacyPolicy(self):
        self.genericobject.PrivacyPolicy()
        result1 = self.genericobject.VerifyPrivacyPolicy()
        self.teststatus.markFinal("test_PrivacyPolicy", result1, "Privacy policy page viewed")

    @pytest.mark.run(order=8)
    def test_ContactUs(self):
        self.genericobject.ContactUs()
        result1 = self.genericobject.VerifyContactUs()
        self.teststatus.markFinal("test_ContactUs", result1, "Contact Us page viewed")

    @pytest.mark.run(order=9)
    def test_Subscription(self):
        self.genericobject.Subscription()
        result1 = self.genericobject.VerifySubscription()
        self.teststatus.markFinal("test_Subscription", result1, "Subscription page viewed")

    @pytest.mark.run(order=10)
    def test_EditProfile(self):
        self.genericobject.EditProfile("Mashruf","Mustavi","DgMarket","01707966823","https://dgmarketbd.com")
        result1 = self.genericobject.verifyEditProfile()
        self.teststatus.markFinal("test_EditProfile", result1, "profile edited successfully")

    @pytest.mark.run(order=11)
    def test_EditProfileFirstNameBlank(self):
        self.genericobject.EditProfileFirstNameBlank("p")
        result1 = self.genericobject.VerifyEditProfileNoFirstname()
        self.teststatus.markFinal("test_EditProfileFirstNameBlank", result1, "Profile not updated and error shown")

    @pytest.mark.run(order=12)
    def test_EditProfileNoLastName(self):
        self.genericobject.EditProfileNoLastName("p")
        result1 = self.genericobject.VerifyEditProfileNoLastName()
        self.teststatus.markFinal("test_EditProfileNoLastName", result1, "Profile not updated and error shown")

    @pytest.mark.run(order=13)
    def test_EditProfileNoOrgName(self):
        self.genericobject.EditProfileNoOrgName("p")
        result1 = self.genericobject.VerifyEditProfileNoOrgName()
        self.teststatus.markFinal("test_EditProfileNoOrgName", result1, "Profile not updated and error shown")

    @pytest.mark.run(order=14)
    def test_EditProfileNoPhone(self):
        self.genericobject.EditProfileNoPhone("p")
        result1 = self.genericobject.VerifyNoPhone()
        self.teststatus.markFinal("test_EditProfileNoPhone", result1, "Profile not updated and error shown")

    @pytest.mark.run(order=15)
    def test_ResetPassword(self):
        self.genericobject.ResetPassword("12345","12345")
        result1 = self.genericobject.VerifyPasswordReset()
        self.teststatus.markFinal("test_ResetPassword", result1, "password reset successful")

    @pytest.mark.run(order=16)
    def test_ResetPasswordNoPass(self):
        self.genericobject.ResetPasswordNoPass("12345")
        result1 = self.genericobject.VerifyResetPasswordNoPass()
        self.teststatus.markFinal("test_ResetPasswordNoPass", result1, "password reset unsuccessful")

    @pytest.mark.run(order=17)
    def test_ResetPasswordNoCOnfirm(self):
        self.genericobject.ResetPasswordNoCOnfirm("12345")
        result1 = self.genericobject.VerifyResetPasswordNoCOnfirm()
        self.teststatus.markFinal("test_ResetPasswordNoCOnfirm", result1, "password reset unsuccessful")

    @pytest.mark.run(order=18)
    def test_ResetPasswordEmpty(self):
        self.genericobject.ResetPasswordEmpty()
        result1 = self.genericobject.VerifyResetPasswordEmpty()
        self.teststatus.markFinal("test_ResetPasswordEmpty", result1, "password reset unsuccessful")

