import unittest
import pytest
from test_page.home.notice_details_page import NoticeDetailsPage
from utilities.teststatus import  TestStatus


@pytest.mark.usefixtures('setup')
class NoticeDetailsTest(unittest.TestCase):
    @pytest.fixture(autouse=True)
    def classSetup(self,setup):
        self.noticeobject = NoticeDetailsPage(self.driver)
        self.teststatus = TestStatus(self.driver)

    @pytest.mark.run(order=1)
    def test_notice_details(self):
        self.noticeobject.NoticeDetails()
        result1 = self.noticeobject.verify_notice_details()
        self.teststatus.markFinal("test_notice_details", result1, "Notice details viewed")

    @pytest.mark.run(order=2)
    def test_emailing(self):
        self.noticeobject.emailing_from_notice_details()
        result1 = self.noticeobject.verify_emailing()
        self.teststatus.markFinal("test_emailing", result1, "Users will be able to send emails")

    @pytest.mark.run(order=3)
    def test_calling(self):
        self.noticeobject.calling_from_notice_details()
        result1 = self.noticeobject.VerifyCalling()
        self.teststatus.markFinal("test_calling", result1, "Users will be able to call")

    @pytest.mark.run(order=4)
    def test_attachment_view(self):
        self.noticeobject.AttachmentView()
        result1 = self.noticeobject.VerifyAttachmentVIew()
        self.teststatus.markFinal("test_attachment_view", result1, "attachment was viewed")

    @pytest.mark.run(order=5)
    def test_doc_view(self):
        self.noticeobject.DocViewInOrgText()
        result1 = self.noticeobject.VerifyDocView()
        self.teststatus.markFinal("test_doc_view", result1, "Doc was viewed under original text of the notice")
    #
    @pytest.mark.run(order=6)
    def test_image_view(self):
        self.noticeobject.ImageAttachment()
        result1 = self.noticeobject.VerifyImageView()
        self.teststatus.markFinal("test_image_view", result1, "Image viewed successfully")

    @pytest.mark.run(order=7)
    def test_expand_orgtext(self):
        self.noticeobject.OrgTextExpand()
        result1 = self.noticeobject.VerifyExpandDownload()
        self.teststatus.markFinal("test_expand_orgtext", result1, "Original text expanded and downloaded")



