from base.selenium_driver import SeleniumDriver
import utilities.custom_logger as cl
from time import sleep
from appium.webdriver.common.touch_action import TouchAction
from selenium import webdriver
import logging

class NewsSearchPage(SeleniumDriver):
    log = cl.customlogger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    #locators
    _news_tab="com.dgmarket.android:id/navigation_news"#id
    _search_icon="com.dgmarket.android:id/searchIcon"#id
    _keyword_field="com.dgmarket.android:id/et_keyword"#id
    _sector_field="com.dgmarket.android:id/tv_notice_category"
    _sector_select="//android.widget.RelativeLayout[@index=4]/android.widget.CheckBox[@index=0]"#xpath
    _sector_select2="//android.widget.RelativeLayout[@index=3]/android.widget.CheckBox[@index=0]"
    _done_button="com.dgmarket.android:id/btn_done"#id
    _countryField="com.dgmarket.android:id/tv_countryLocal"#id
    _select_country1="//android.widget.RelativeLayout[@index=3]/android.widget.CheckBox[@index=0]"#xpath
    _select_country2="//android.widget.RelativeLayout[@index=1]/android.widget.CheckBox[@index=0]"
    _search_button="com.dgmarket.android:id/btn_search"#id
    _search_result="//android.widget.LinearLayout[@index=0]/android.widget.TextView[@index=0]"#xpath
    _no_result="com.dgmarket.android:id/iv_no_content"#id
    _funding_agency_field="com.dgmarket.android:id/tv_funding_agency"
    _funding_agency_select="//android.widget.TextView[@text='African Development Bank (AfDB)']"
    _funding_search_result="//android.widget.LinearLayout[@index=3]/android.widget.TextView[@index=1]"


    #methods
    def ClickNewsTab(self):
        self.elementClick(self._news_tab,locatorType='id')
    def ClickSearchIcon(self):
        self.elementClick(self._search_icon,locatorType='id')
    def TypeKeyword(self,keyword):
        self.sendKeys(keyword,self._keyword_field)
    def ClickSectorField(self):
        self.elementClick(self._sector_field,locatorType='id')
    def SectorSelect1(self):
        self.elementClick(self._sector_select,locatorType='xpath')
    def SectorSelect2(self):
        self.elementClick(self._sector_select2,locatorType='xpath')
    def ClickDoneButton(self):
        self.elementClick(self._done_button,locatorType='id')
    def ClickCountryField(self):
        self.elementClick(self._countryField,locatorType='id')
    def SelectCountry(self):
        self.elementClick(self._select_country1,locatorType='xpath')
    def SelectCountry2(self):
        self.elementClick(self._select_country2,locatorType='xpath')
    def ClickSearchButton(self):
        self.elementClick(self._search_button,locatorType='id')
    def ClickFundingAgencyfield(self):
        self.elementClick(self._funding_agency_field,locatorType='id')
    def SelectFundingAgency(self):
        self.elementClick(self._funding_agency_select,locatorType='xpath')



    def SimpleSearch(self,keyword):
        self.driver.implicitly_wait(10)
        self.ClickNewsTab()
        self.driver.implicitly_wait(10)
        self.ClickSearchIcon()
        self.driver.implicitly_wait(10)
        self.TypeKeyword(keyword)
        self.driver.implicitly_wait(10)
        self.ClickSectorField()
        self.driver.implicitly_wait(10)
        self.SectorSelect1()
        # self.driver.implicitly_wait(10)
        # self.SectorSelect2()
        self.driver.implicitly_wait(10)
        self.ClickDoneButton()
        # self.driver.implicitly_wait(10)
        # self.ClickCountryField()
        # self.driver.implicitly_wait(10)
        # self.SelectCountry()
        # self.driver.implicitly_wait(10)
        # self.ClickDoneButton()
        self.driver.implicitly_wait(10)
        self.ClickSearchButton()

    def verifySimpleSearch(self):
        sleep(5)
        result=self.isElementPresent(self._search_result,locatorType="xpath")
        return result

    def SearchNoResult(self,keyword):
        self.driver.implicitly_wait(10)
        self.ClickNewsTab()
        self.driver.implicitly_wait(10)
        self.ClickSearchIcon()
        self.driver.implicitly_wait(10)
        self.TypeKeyword(keyword)
        self.driver.implicitly_wait(10)
        self.ClickSectorField()
        self.driver.implicitly_wait(10)
        self.SectorSelect1()
        self.driver.implicitly_wait(10)
        self.ClickDoneButton()
        self.driver.implicitly_wait(10)
        self.ClickCountryField()
        self.driver.implicitly_wait(10)
        self.SelectCountry()
        self.driver.implicitly_wait(10)
        self.ClickDoneButton()
        self.driver.implicitly_wait(10)
        self.ClickSearchButton()

    def verifySearchNoResult(self):
        sleep(5)
        result=self.isElementPresent(self._no_result,locatorType="id")
        return result

    def SearchMultipleSectors(self):
        self.driver.implicitly_wait(10)
        self.ClickNewsTab()
        self.driver.implicitly_wait(10)
        self.ClickSearchIcon()
        self.driver.implicitly_wait(10)
        self.ClickSectorField()
        self.driver.implicitly_wait(10)
        self.SectorSelect1()
        self.driver.implicitly_wait(10)
        self.SectorSelect2()
        self.driver.implicitly_wait(10)
        self.ClickDoneButton()
        self.driver.implicitly_wait(10)
        self.ClickSearchButton()

    def verifySearchMultipleSecrtors(self):
        sleep(5)
        result=self.isElementPresent(self._search_result,locatorType="xpath")
        return result

    def SearchMultipleCountries(self):
        self.driver.implicitly_wait(10)
        self.ClickNewsTab()
        self.driver.implicitly_wait(10)
        self.ClickSearchIcon()
        self.driver.implicitly_wait(10)
        self.ClickCountryField()
        self.driver.implicitly_wait(10)
        self.SelectCountry()
        self.driver.implicitly_wait(10)
        self.SelectCountry2()
        self.driver.implicitly_wait(10)
        self.ClickDoneButton()
        self.driver.implicitly_wait(10)
        self.ClickSearchButton()

    def verifySearchMultipleCOuntries(self):
        sleep(5)
        result = self.isElementPresent(self._search_result, locatorType="xpath")
        return result

    def SearchFundingAgency(self):
        self.driver.implicitly_wait(10)
        self.ClickNewsTab()
        self.driver.implicitly_wait(10)
        self.ClickSearchIcon()
        self.driver.implicitly_wait(10)
        self.ClickFundingAgencyfield()
        sleep(5)
        for i in range(0,1):
            actions = TouchAction(self.driver)
            actions.press(x=542.0, y=716.0).move_to(x=546, y=670).release().perform()
        self.driver.implicitly_wait(10)
        self.SelectFundingAgency()
        self.driver.implicitly_wait(10)
        self.ClickSearchButton()

    def VerifyFundingSearch(self):
        sleep(5)
        result=self.isElementPresent(self._funding_search_result,locatorType='xpath')
        return result