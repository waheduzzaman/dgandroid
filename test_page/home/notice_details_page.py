from base.selenium_driver import SeleniumDriver
import utilities.custom_logger as cl
from time import sleep
from appium.webdriver.common.touch_action import TouchAction
import logging
from selenium.webdriver.support.ui import WebDriverWait

import pytest

class NoticeDetailsPage(SeleniumDriver):
    log = cl.customlogger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    #locators
    _first_notice="com.dgmarket.android:id/tv_notice_title"#id
    _seen_text="//android.widget.LinearLayout[@index=0]//android.widget.TextView[@text='Seen']"#xpath
    _notice_view_web="com.dgmarket.android:id/tv_seen"#id
    _just_once="android:id/button_once"#id
    _save_icon="com.dgmarket.android:id/tv_save_notice"#id
    _share_notice="com.dgmarket.android:id/tv_share_notice"#id
    #_notice_details="//android.widget.RelativeLayout[@index=0]/android.widget.TextView[@text='Details']"#xpath
    _notice_details="com.dgmarket.android:id/ll_open_web"
    _wishlist_icon_drawer="com.dgmarket.android:id/navigation_wishlist"#id
    _wishlist_notice_select="//android.widget.TextView[@text='Rfp/Undp/Hai/19.095 Caractérisation De L’État De Santé Des Ecosystèmes Côtiers Et Marins']"#xpath
    _email_under_wish_notice="com.dgmarket.android:id/tv_email"#id
    _select_gmail="//android.widget.FrameLayout[@index=0]/android.widget.ImageView[@index=0]"#id
    _email_sending_form="com.google.android.gm:id/add_attachment"#id
    #_notice_for_phone="//android.widget.LinearLayout[@index=1]/android.widget.TextView[@text='2019-04-25 ']"
    _notice_for_phone="//android.widget.LinearLayout[@index=1]/android.widget.TextView[@text='BANGLADESH TELECOMMUNICATION REGULATORY COMMISSION (BTRC)']"
    _click_phone="//android.widget.FrameLayout[@index=0]/android.widget.ImageView[@index=0]"#xpath
    _phone_number="//android.widget.TextView[@text='02-9554479']"
    _verify_phone="//android.widget.EditText[@text='029554479']"#xpath
    _chrome_icon="//android.widget.TextView[@text='Chrome']"
    _attachment="//android.view.View[@index=0]/android.view.View[@index=3]"
    _verify_attachment_view="com.android.chrome:id/infobar_message"#id
    _doc_originalText="//android.webkit.WebView[@index=0]/android.view.View[@index=2]"
    _image_attachment="com.dgmarket.android:id/tv"#id
    _verify_image_view="com.dgmarket.android:id/iv_image"#id
    _Original_text_expand="com.dgmarket.android:id/tv_notice_body_header"#id
    _download_button = "com.dgmarket.android:id/ivDownload"  # id
    _allow_button="com.android.packageinstaller:id/permission_allow_button"#id




    #Methods

    def clickFirstNotice(self):
        self.elementClick(self._first_notice, locatorType="id")
    def clickNoticeViewWeb(self):
        self.elementClick(self._notice_view_web,locatorType="id")  # elementClick is a method called from seleniumDriver class
    def clickJustOnce(self):
        self.elementClick(self._just_once,locatorType="id")
    def clickSaveIcon(self):
        self.elementClick(self._save_icon, locatorType="id")
    def clickShareNotice(self):
        self.elementClick(self._share_notice, locatorType="id")
    def clickwishlisticon(self):
        self.elementClick(self._wishlist_icon_drawer,locatorType="id")
    def clickFirstWIshlistNotice(self):
        self.elementClick(self._wishlist_notice_select,locatorType="xpath")
    def clickEmailUnderWishNotice(self):
        self.elementClick(self._email_under_wish_notice,locatorType="id")
    def selectGmail(self):
        self.elementClick(self._select_gmail,locatorType="xpath")
    def ClickWishlistNoticeForPhone(self):
        self.elementClick(self._notice_for_phone,locatorType='xpath')
    def ClickPhoneNumber(self):
        self.elementClick(self._phone_number,locatorType='xpath')
    def ClickChrome(self):
        self.elementClick(self._chrome_icon,locatorType='xpath')
    def ClickPhone(self):
        self.elementClick(self._click_phone,locatorType='xpath')
    def ClickAttachment(self):
        self.elementClick(self._attachment,locatorType='xpath')
    def ClickDownloadButton(self):
        self.elementClick(self._download_button,locatorType='id')
    def ClickDocInOrgtext(self):
        self.elementClick(self._doc_originalText,locatorType='xpath')
    def ClickImageAttachment(self):
        self.elementClick(self._image_attachment,locatorType='id')
    def ClickOrgtextExpand(self):
        self.elementClick(self._Original_text_expand,locatorType='id')
    def ClickDOnwloadButton(self):
        self.elementClick(self._download_button,locatorType="id")
    def ClickAllowButton(self):
        self.elementClick(self._allow_button,locatorType='id')


    def NoticeDetails(self):
        self.driver.implicitly_wait(10)
        self.clickFirstNotice()
        #check if the notice seen status can be found
        sleep(4)
        self.driver.back()
        self.driver.implicitly_wait(4)
        self.isElementPresent(self._seen_text,locatorType="xpath")

        #Notice view on web
        self.driver.implicitly_wait(5)
        self.clickFirstNotice()
        self.driver.implicitly_wait(7)
        self.clickNoticeViewWeb()
        # sleep(5)
        # self.clickJustOnce()
        sleep(7)
        for i in range(0,30):
                actions = TouchAction(self.driver)
                #actions.press(x=399.9, y=1691.9).move_to(x=914.9, y=1561.9).release().perform()
                actions.press(x=590.0, y=1242.).move_to(x=0, y=0).release().perform()

        #notice save icon click from notice details
        self.driver.implicitly_wait(10)
        self.driver.back()
        self.driver.implicitly_wait(10)
        self.clickSaveIcon()
        self.driver.implicitly_wait(10)
        self.clickSaveIcon()


        #test share notice
        self.driver.implicitly_wait(10)
        self.clickShareNotice()

        #Scroll down on notice details page
        self.driver.implicitly_wait(2)
        self.driver.back()
        sleep(5)
        for i in range(0,7):
                self.driver.implicitly_wait(3)
                actions = TouchAction(self.driver)
                actions.press(x=582.0,y=1084.0).move_to(x=0,y=0).release().perform()
        sleep(5)
        for i in range(0,7):
                self.driver.implicitly_wait(3)
                actions = TouchAction(self.driver)
                actions.press(x=584.0,y=632.0).move_to(x=545,y=1094).release().perform()
        self.driver.back()

    #verification
    def verify_notice_details(self):
        sleep(10)
        result = self.isElementPresent(self._seen_text,locatorType="xpath")
        return result

    def emailing_from_notice_details(self):
        self.driver.implicitly_wait(10)
        #click on wishlist tab
        self.clickwishlisticon()
        #view the first wishlist notice
        self.driver.implicitly_wait(2)
        self.clickFirstWIshlistNotice()

        #click on the email address under noticed details
        self.driver.implicitly_wait(5)
        self.clickEmailUnderWishNotice()

        #Get the gmail email form
        sleep(5)
        self.selectGmail()

    #verification
    def verify_emailing(self):
        sleep(5)
        result = self.isElementPresent(self._email_sending_form, locatorType="id")
        return result

    def calling_from_notice_details(self):
        self.driver.implicitly_wait(10)
        self.clickwishlisticon()
        # sleep(5)
        # for i in range(0,7):
        #         self.driver.implicitly_wait(3)
        #         actions = TouchAction(self.driver)
        #         actions.press(x=869.0,y=1493.9).move_to(x=0,y=0).release().perform()
        sleep(5)
        self.ClickWishlistNoticeForPhone()
        self.driver.implicitly_wait(10)
        self.ClickPhoneNumber()
        self.driver.implicitly_wait(10)
        #self.ClickPhone()

    def VerifyCalling(self):
        sleep(5)
        result = self.isElementPresent(self._verify_phone, locatorType="xpath")
        return result

    def AttachmentView(self):
        self.driver.implicitly_wait(10)
        # click on wishlist tab
        self.clickwishlisticon()
        # view the first wishlist notice
        self.driver.implicitly_wait(2)
        self.clickFirstWIshlistNotice()
        self.driver.implicitly_wait(10)
        self.ClickAttachment()

    def VerifyAttachmentVIew(self):
        sleep(3)
        result=self.isElementPresent(self._verify_attachment_view,locatorType='id')
        return result

    def DocViewInOrgText(self):
        self.driver.implicitly_wait(10)
        # click on wishlist tab
        self.clickwishlisticon()
        # view the first wishlist notice
        self.driver.implicitly_wait(2)
        self.clickFirstWIshlistNotice()
        sleep(5)
        for i in range(0,3):
                actions = TouchAction(self.driver)
                #actions.press(x=399.9, y=1691.9).move_to(x=914.9, y=1561.9).release().perform()
                actions.press(x=566.0, y=1053.0).move_to(x=0, y=0).release().perform()
        sleep(5)
        self.ClickDocInOrgtext()

    def VerifyDocView(self):
        sleep(5)
        result=self.isElementPresent(self._verify_attachment_view,locatorType='id')
        return result

    def ImageAttachment(self):
        self.driver.implicitly_wait(10)
        self.clickwishlisticon()
        # sleep(5)
        # for i in range(0,7):
        #         self.driver.implicitly_wait(3)
        #         actions = TouchAction(self.driver)
        #         actions.press(x=869.0,y=1493.9).move_to(x=0,y=0).release().perform()
        sleep(5)
        self.ClickWishlistNoticeForPhone()
        self.driver.implicitly_wait(10)
        self.ClickImageAttachment()

    def VerifyImageView(self):
        sleep(5)
        result=self.isElementPresent(self._verify_image_view,locatorType='id')
        return result

    def OrgTextExpand(self):
        self.driver.implicitly_wait(10)
        # click on wishlist tab
        self.clickwishlisticon()
        # view the first wishlist notice
        self.driver.implicitly_wait(2)
        self.clickFirstWIshlistNotice()
        sleep(5)
        for i in range(0,7):
                self.driver.implicitly_wait(3)
                actions = TouchAction(self.driver)
                actions.press(x=582.0,y=1084.0).move_to(x=0,y=0).release().perform()
        self.driver.implicitly_wait(10)
        self.ClickOrgtextExpand()
        sleep(5)
        self.ClickDownloadButton()
        self.driver.implicitly_wait(10)
        self.ClickAllowButton()


    def VerifyExpandDownload(self):
        sleep(5)
        result=self.isElementPresent(self._download_button,locatorType='id')
        return result






