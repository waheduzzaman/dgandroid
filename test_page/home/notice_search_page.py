from base.selenium_driver import SeleniumDriver
import utilities.custom_logger as cl
from time import sleep
from appium.webdriver.common.touch_action import TouchAction
from selenium import webdriver
import logging

class NoticeSearchPage(SeleniumDriver):
    log = cl.customlogger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    #locators
    _search_icon="com.dgmarket.android:id/searchIcon"#id
    _back_icon="com.dgmarket.android:id/iv_back_icon"#id
    _keyword="com.dgmarket.android:id/et_keyword"#id
    _keyword_text="//android.widget.TextView[@text='Keyword']"
    _keyword_input_field="com.dgmarket.android:id/et_buyer_name"#id
    _keyword_input_field2="com.dgmarket.android:id/tv_buyer_name"
    _suggestion_keyword="//android.widget.LinearLayout[@index=0]/android.widget.TextView[@index=0]"#xpath
    _suggestion_keyword2="//android.widget.LinearLayout[@index=0]/android.widget.TextView[@index=0]"
    _cpv="com.dgmarket.android:id/tv_cpv_search"#id
    _cpv_field="com.dgmarket.android:id/et_cpv"#id
    _dropdown_cpv1="//android.widget.LinearLayout[@index=0]/android.widget.TextView[@index=0]"
    _dropdown_cpv2 = "//android.widget.LinearLayout[@index=1]/android.widget.TextView[@index=0]"  # xpath
    _CPVchip1="//android.widget.RelativeLayout[@index=1]/android.widget.ImageView[@index=1]"#id
    _done_button="com.dgmarket.android:id/btn_okay"#id
    _buyer_name="com.dgmarket.android:id/tv_buyer_search"#id
    _buyer_name_input_field="com.dgmarket.android:id/et_buyer_name"#id
    _buyer_name_suggestion="//android.widget.LinearLayout[@index=0]/android.widget.TextView[@index=1]"#xpath
    _funding_agency="com.dgmarket.android:id/tv_funding_agency"#id
    _notice_type="com.dgmarket.android:id/tv_notice_type"#id
    _NoticeType_in_for_bid="//android.widget.TextView[@text='Invitation for Bids']"#xpath
    _sector="com.dgmarket.android:id/tv_notice_category"#id
    _country="com.dgmarket.android:id/tv_countryLocal"#id
    _country_select="//android.widget.RelativeLayout[@index=1]/android.widget.CheckBox[@index=0]"
    _sector_done="com.dgmarket.android:id/btn_done"#id
    _search_button="com.dgmarket.android:id/btn_search"#id
    _search_result="com.dgmarket.android:id/tv_notice_title"#id
    _sector_select="//android.widget.RelativeLayout[@index=3]/android.widget.CheckBox[@index=0]"
    _search_result2="com.dgmarket.android:id/tv_notice_title"#id
    _clear_button="com.dgmarket.android:id/btn_clear_all"#id
    _no_content="com.dgmarket.android:id/iv_no_content"#id
    _publication_start="com.dgmarket.android:id/tv_start_date"#id
    _startDateSelect_From_Picker="//android.view.View[@index=1]/android.view.View[@index=0]"#xpath
    _ok_button="android:id/button1"#id
    #_endDateField="//android.widget.LinearLayout[@index=15]/android.widget.TextView[@index=1]"#id
    _endDateField="com.dgmarket.android:id/tv_end_date"#id
    _endDateFromPicker="//android.view.View[@index=1]/android.view.View[@index=2]"
    _searchResult3="//android.widget.LinearLayout[@index=1]/android.widget.TextView[@text='2019-07-03 ']"
    _search_button1="//android.widget.Button[@text='SEARCH']"
    _min_amount="com.dgmarket.android:id/etAmountMin"#id
    _max_amount="com.dgmarket.android:id/etAmountMax"#id
    _currency="com.dgmarket.android:id/spCurrency"#id
    _currency_bdt="//android.widget.ListView[@index=0]/android.widget.TextView[@index=3]"
    _search_result4="//android.widget.LinearLayout[@index=1]/android.widget.TextView[@index=1]"
    _verify_publish="//android.widget.LinearLayout[@index=0]/android.widget.TextView[@index=0]"
    _search_result5="com.dgmarket.android:id/ll_open_web"
    _back_icon1="com.dgmarket.android:id/iv_back_icon"
    _btn_done="com.dgmarket.android:id/btn_okay"#id
    _verify_buyer="//android.widget.LinearLayout[@index=2]/android.widget.LinearLayout[@index=1]/android.widget.TextView[@text='World Bank']"




    def clickSearchIcon(self):
        self.elementClick(self._search_icon, locatorType="id")
    def ClickBackButton(self):
        self.elementClick(self._back_icon,locatorType="id")
    def ClickKeyword(self):
        self.elementClick(self._keyword,locatorType='id')
    def CLearKeyboardField(self):
        self.elementClick(self._keyword_input_field,locatorType='id')
    def TypeKeyword(self,keyword):
        self.sendKeys(keyword,self._keyword_input_field)
    def ClickSuggestionKeyword(self):
        self.elementClick(self._suggestion_keyword,locatorType='xpath')
    def ClickSuggestionKeyword2(self):
        self.elementClick(self._suggestion_keyword2,locatorType='xpath')
    def clickCPVFIeld(self):
        self.elementClick(self._cpv,locatorType='id')
    def TypeCPV(self,cpv):
        self.sendKeys(cpv,self._cpv_field)
    def clickDropdownCPV1(self):
        self.elementClick(self._dropdown_cpv1,locatorType='xpath')
    def clickDropdownCPV2(self):
        self.elementClick(self._dropdown_cpv2, locatorType='xpath')
    def clickCrossIcon(self):
        self.elementClick(self._CPVchip1,locatorType='xpath')
    def clickDoneButton(self):
        self.elementClick(self._done_button,locatorType='id')
    def ClickBuyerName(self):
        self.elementClick(self._buyer_name)
    def TypeBuyerName(self,buyername):
        self.sendKeys(buyername,self._buyer_name_input_field)
    def ClickSuggestionBuyer(self):
        self.elementClick(self._buyer_name_suggestion,locatorType='xpath')
    def clickNoticeType(self):
        self.elementClick(self._notice_type,locatorType='id')
    def clickInForBid(self):
        self.elementClick(self._NoticeType_in_for_bid,locatorType='xpath')
    def clickSector(self):
        self.elementClick(self._sector,locatorType='id')
    def SectorSelect(self):
        self.elementClick(self._sector_select,locatorType='xpath')
    def clickCOuntry(self):
        self.elementClick(self._country,locatorType='id')
    def SelectCOuntry(self):
        self.elementClick(self._country_select,locatorType='xpath')
    def clickSectorDOneButton(self):
        self.elementClick(self._sector_done,locatorType='id')
    def ClickSearchButton(self):
        self.elementClick(self._search_button,locatorType='id')
    def SelectSector(self):
        self.elementClick(self._sector_select,locatorType='xpath')
    def clickCLearButton(self):
        self.elementClick(self._clear_button,locatorType='id')
    def publicationStart(self):
        self.elementClick(self._publication_start,locatorType='id')
    def SelectStartDate(self):
        self.elementClick(self._startDateSelect_From_Picker,locatorType='xpath')
    def ClickOkButton(self):
        self.elementClick(self._ok_button,locatorType='id')
    def ClickEndDateField(self):
        self.elementClick(self._endDateField,locatorType='id')
    def SelectEndDate(self):
        self.elementClick(self._endDateFromPicker,locatorType='xpath')
    def ClickSearchButtonALt(self):
        self.elementClick(self._search_button1,locatorType='xpath')
    def SendMinAmount(self,min):
        self.sendKeys(min,self._min_amount)
    def SendMaxAMount(self,max):
        self.sendKeys(max,self._max_amount)
    def clickcurrency(self):
        self.elementClick(self._currency,locatorType='id')
    def clickCUrrencyBDT(self):
        self.elementClick(self._currency_bdt,locatorType='xpath')
    def ClickOk(self):
        self.elementClick(self._ok_button,locatorType='id')
    def ClickNoticeAfterSearch(self):
        self.elementClick(self._search_result,locatorType='id')
    def ClickBackIcon(self):
        self.elementClick(self._back_icon1,locatorType='id')
    def ClickDOne(self):
        self.elementClick(self._btn_done,locatorType='id')




    def SimpleSearch(self,keyword,cpv):
        self.driver.implicitly_wait(10)
        self.clickSearchIcon()
        self.driver.implicitly_wait(10)
        self.ClickBackButton()
        self.driver.implicitly_wait(10)
        self.clickSearchIcon()
        self.driver.implicitly_wait(10)
        self.clickCLearButton()
        self.driver.implicitly_wait(10)

        #keyword input
        self.ClickKeyword()
        self.driver.press_keycode(67)
        self.driver.implicitly_wait(10)
        self.TypeKeyword(keyword)
        sleep(5)
        self.ClickSuggestionKeyword()

        #cpv input
        self.driver.implicitly_wait(10)
        self.clickCPVFIeld()
        self.driver.implicitly_wait(10)
        self.ClickBackIcon()
        self.driver.implicitly_wait(10)
        self.clickCPVFIeld()
        self.driver.implicitly_wait(10)
        self.TypeCPV(cpv)
        #self.driver.hide_keyboard()
        sleep(3)
        self.clickDropdownCPV1()
        sleep(3)
        self.clickDropdownCPV2()
        self.driver.implicitly_wait(10)
        self.clickCrossIcon()
        self.driver.implicitly_wait(10)
        self.clickDoneButton()

        # #buyername insert
        # self.driver.implicitly_wait(10)
        # self.ClickBuyerName()
        # self.driver.implicitly_wait(10)
        # self.TypeBuyerName(buyername)
        # sleep(5)
        # self.ClickDOne()
        # self.driver.implicitly_wait(10)
        #self.driver.hide_keyboard()

        #notice type select
        self.driver.implicitly_wait(10)
        self.clickNoticeType()
        sleep(5)
        for i in range(0,15):
                actions = TouchAction(self.driver)
                actions.press(x=508.0, y=894.0).move_to(x=0, y=0).release().perform()
        self.driver.implicitly_wait(10)
        self.clickInForBid()

        #search initialize
        self.driver.implicitly_wait(10)
        self.ClickSearchButton()

    def verifySimpleSearch(self):
        self.driver.implicitly_wait(10)
        result = self.isElementPresent(self._search_result, locatorType="id")
        return result

    def SimpleSearch2(self,keyword):
        self.driver.implicitly_wait(10)
        self.clickSearchIcon()
        self.driver.implicitly_wait(10)
        self.clickCLearButton()
        self.driver.implicitly_wait(10)
        self.ClickKeyword()
        self.driver.press_keycode(67)
        self.driver.implicitly_wait(10)
        self.TypeKeyword(keyword)
        #self.driver.hide_keyboard()
        sleep(5)
        self.ClickSuggestionKeyword2()
        # self.driver.implicitly_wait(10)
        # #self.driver.hide_keyboard()
        # sleep(5)
        # for i in range(0,25):
        #         actions = TouchAction(self.driver)
        #         actions.press(x=736.0, y=1231.9).move_to(x=770.0, y=1132.9).release().perform()
        sleep(3)
        self.clickSector()
        self.driver.implicitly_wait(10)
        self.SelectSector()
        self.driver.implicitly_wait(10)
        self.clickSectorDOneButton()
        self.driver.implicitly_wait(10)
        self.clickCOuntry()
        sleep(3)
        self.SelectCOuntry()
        self.driver.implicitly_wait(10)
        self.clickSectorDOneButton()
        self.driver.implicitly_wait(10)
        self.ClickSearchButton()

    def verifySimpleSearch2(self):
            self.driver.implicitly_wait(10)
            result = self.isElementPresent(self._search_result2, locatorType="id")
            return result

    def SearchNoResult(self,keyword):
        self.driver.implicitly_wait(10)
        self.clickSearchIcon()
        self.driver.implicitly_wait(10)

        # keyword input
        self.ClickKeyword()
        self.driver.press_keycode(67)
        self.driver.implicitly_wait(10)
        self.TypeKeyword(keyword)
        self.driver.implicitly_wait(10)
        self.clickDoneButton()
        #self.driver.hide_keyboard()

        #Search Initialize
        self.driver.implicitly_wait(10)
        self.ClickSearchButton()

    def verifySearchNoResult(self):
        self.driver.implicitly_wait(10)
        result = self.isElementPresent(self._no_content, locatorType="id")
        return result


    def SearchEstimatedVAlue(self,min,max):
            self.driver.implicitly_wait(10)
            self.clickSearchIcon()
            self.driver.implicitly_wait(10)

            sleep(5)
            for i in range(0, 25):
                actions = TouchAction(self.driver)
                actions.press(x=623.0, y=1191.0).move_to(x=0, y=0).release().perform()
            self.driver.implicitly_wait(10)
            self.SendMinAmount(min)
            #self.driver.hide_keyboard()
            self.driver.implicitly_wait(10)
            self.SendMaxAMount(max)
            #self.driver.hide_keyboard()
            self.driver.implicitly_wait(10)
            self.clickcurrency()
            self.driver.implicitly_wait(10)
            self.clickCUrrencyBDT()
            self.driver.implicitly_wait(10)
            self.ClickSearchButton()

    def verifyEstimatedValueSearch(self):
        self.driver.implicitly_wait(10)
        result = self.isElementPresent(self._search_result4, locatorType="xpath")
        return result

    def PublishedDateSearch(self):
        self.driver.implicitly_wait(10)
        self.clickSearchIcon()
        self.driver.implicitly_wait(10)
        self.clickCLearButton()
        sleep(5)
        for i in range(0, 25):
            actions = TouchAction(self.driver)
            actions.press(x=623.0, y=1191.0).move_to(x=0, y=0).release().perform()

        #Publication Start
        sleep(3)
        self.publicationStart()
        self.driver.implicitly_wait(10)
        self.SelectStartDate()
        self.driver.implicitly_wait(10)
        self.ClickOk()
        sleep(5)
        for i in range(0,7):
                self.driver.implicitly_wait(3)
                actions = TouchAction(self.driver)
                actions.press(x=543.0,y=1033.0).move_to(x=578.0,y=629.0).release().perform()


        # search initialize
        self.driver.implicitly_wait(10)
        self.ClickSearchButton()

    def VerifyPublishSearch(self):
        sleep(5)
        result=self.isElementPresent(self._verify_publish,locatorType="xpath")
        return result


    def PublishedEndDateSearch(self):
        self.driver.implicitly_wait(10)
        self.clickSearchIcon()
        self.driver.implicitly_wait(10)
        self.clickCLearButton()
        sleep(5)
        for i in range(0, 25):
            actions = TouchAction(self.driver)
            actions.press(x=623.0, y=1191.0).move_to(x=0, y=0).release().perform()

        #publication end
        self.ClickEndDateField()
        self.driver.implicitly_wait(10)
        self.SelectEndDate()
        self.driver.implicitly_wait(10)
        self.ClickOk()
        for i in range(0, 7):
                self.driver.implicitly_wait(3)
                actions = TouchAction(self.driver)
                actions.press(x=543.0, y=1033.0).move_to(x=578.0, y=629.0).release().perform()

        # search initialize
        self.driver.implicitly_wait(10)
        self.ClickSearchButton()

    def VerifyEndPublishDate(self):
        sleep(5)
        result=self.isElementPresent(self._verify_publish,locatorType="xpath")
        return result

    def NoticeDetailsViewAfterSearch(self,keyword,cpv):
        self.driver.implicitly_wait(10)
        self.clickSearchIcon()
        self.driver.implicitly_wait(10)
        self.ClickBackButton()
        self.driver.implicitly_wait(10)
        self.clickSearchIcon()
        self.driver.implicitly_wait(10)
        self.clickCLearButton()
        self.driver.implicitly_wait(10)

        # keyword input
        self.ClickKeyword()
        self.driver.press_keycode(67)
        self.driver.implicitly_wait(10)
        self.TypeKeyword(keyword)
        sleep(3)
        self.ClickSuggestionKeyword()

        # cpv input
        self.driver.implicitly_wait(10)
        self.clickCPVFIeld()
        self.driver.implicitly_wait(10)
        self.ClickBackIcon()
        self.driver.implicitly_wait(10)
        self.clickCPVFIeld()
        self.driver.implicitly_wait(10)
        self.TypeCPV(cpv)
        # self.driver.hide_keyboard()
        sleep(3)
        self.clickDropdownCPV1()
        sleep(3)
        self.clickDropdownCPV2()
        self.driver.implicitly_wait(10)
        self.clickCrossIcon()
        self.driver.implicitly_wait(10)
        self.clickDoneButton()

        # # buyername insert
        # self.driver.implicitly_wait(10)
        # self.ClickBuyerName()
        # self.driver.implicitly_wait(10)
        # self.TypeBuyerName(buyername)
        # sleep(5)
        # self.ClickDOne()
        # self.driver.implicitly_wait(10)
        # # self.driver.hide_keyboard()

        # notice type select
        self.driver.implicitly_wait(10)
        self.clickNoticeType()
        sleep(5)
        for i in range(0, 15):
            actions = TouchAction(self.driver)
            actions.press(x=508.0, y=894.0).move_to(x=0, y=0).release().perform()
        self.driver.implicitly_wait(10)
        self.clickInForBid()

        # search initialize
        self.driver.implicitly_wait(10)
        self.ClickSearchButton()
        sleep(2)
        self.ClickNoticeAfterSearch()


    def VerifyNoticeDetailsViewAfterSearch(self):
        sleep(5)
        result=self.isElementPresent(self._search_result5,locatorType="id")
        return result

    def SearchWIthBuyerName(self,buyername):

        self.driver.implicitly_wait(10)
        self.clickSearchIcon()

        #buyername insert
        self.driver.implicitly_wait(10)
        self.ClickBuyerName()
        self.driver.implicitly_wait(10)
        self.TypeBuyerName(buyername)
        sleep(5)
        self.ClickSuggestionBuyer()
        self.driver.implicitly_wait(10)
        self.driver.implicitly_wait(10)
        self.ClickSearchButton()

    def VerifySearchBuyerName(self):
        sleep(5)
        result=self.isElementPresent(self._verify_buyer,locatorType='xpath')
        return result




