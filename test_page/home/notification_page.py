from base.selenium_driver import SeleniumDriver
import utilities.custom_logger as cl
from time import sleep
from appium.webdriver.common.touch_action import TouchAction
import logging
from selenium.webdriver.support.ui import WebDriverWait

import pytest

class Notifications(SeleniumDriver):
    log = cl.customlogger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    #locators
    _notification_tab="com.dgmarket.android:id/navigation_notification"#id
    _notification_card="//android.widget.FrameLayout[@index=0]"#xpath
    _criteria="com.dgmarket.android:id/tv_criteria_name"#id
    _result="com.dgmarket.android:id/tv_notice_title"#id

    #methods
    def ClickNotificationTab(self):
        self.elementClick(self._notification_tab,locatorType='id')
    def ClickNotiCard(self):
        self.elementClick(self._notification_card,locatorType='xpath')
    def ClickCriteria(self):
        self.elementClick(self._criteria,locatorType='id')


    def NotificationView(self):
        self.driver.implicitly_wait(10)
        self.ClickNotificationTab()
        self.driver.implicitly_wait(10)
        self.ClickNotiCard()
        sleep(8)
        self.ClickCriteria()

    def VerifyNotiView(self):
        sleep(5)
        result=self.isElementPresent(self._result,locatorType='id')
        return result
