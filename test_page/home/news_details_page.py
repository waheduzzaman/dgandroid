from base.selenium_driver import SeleniumDriver
import utilities.custom_logger as cl
from time import sleep
from appium.webdriver.common.touch_action import TouchAction
from selenium import webdriver
import logging

class NewsDetailsPage(SeleniumDriver):
    log = cl.customlogger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    #locators
    _news_tab = "com.dgmarket.android:id/navigation_news"  # id
    _news_card="//android.widget.LinearLayout[@index=0]/android.widget.TextView[@index=0]"
    _news_details="//android.widget.LinearLayout[@index=0]/android.widget.TextView[@index=4]"

    #methods
    def ClickNewsTab(self):
        self.elementClick(self._news_tab,locatorType='id')
    def ClickNewsCard(self):
        self.elementClick(self._news_card,locatorType='xpath')



    def NewsDetails(self):
        self.driver.implicitly_wait(10)
        self.ClickNewsTab()
        self.driver.implicitly_wait(10)
        self.ClickNewsCard()
        sleep(5)
        for i in range(0,3):
                actions = TouchAction(self.driver)
                actions.press(x=579.0, y=1208.0).move_to(x=0, y=0).release().perform()

    def VerifyNewsDetails(self):
        sleep(5)
        result=self.isElementPresent(self._news_details,locatorType='xpath')
        return result