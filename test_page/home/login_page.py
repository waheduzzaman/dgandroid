from base.selenium_driver import SeleniumDriver
import utilities.custom_logger as cl
from time import sleep
from appium.webdriver.common.touch_action import TouchAction
import logging
from selenium.webdriver.support.ui import WebDriverWait

import pytest

class LoginPage(SeleniumDriver):
    log = cl.customlogger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    #locators
    _skip_button="com.dgmarket.android:id/tv_skip"#id
    _email= "com.dgmarket.android:id/et_user_email"#id
    _password="com.dgmarket.android:id/et_user_password"#id
    _sign_in_button="com.dgmarket.android:id/btn_signin"#id
    _forgot_password = "com.dgmarket.android:id/tv_forgot_password"
    _search_icon="com.dgmarket.android:id/searchIcon"

    #methods
    def clickSkipButton(self):
        self.elementClick(self._skip_button, locatorType="id")

    def ClickEmailField(self):
        self.elementClick(self._email,locatorType="id")

    def clickemailfield(self):
        self.elementClick(self._email,locatorType='id')

    def TypeEmail(self,email):
        self.sendKeys(email,self._email)

    def TypePassword(self,password):
        self.sendKeys(password,self._password)

    def clickSignInButton(self):
        self.elementClick(self._sign_in_button, locatorType="id")


    def FailedLoginWithoutPass(self,email):
        self.driver.implicitly_wait(10)
        self.clickSkipButton()
        self.driver.implicitly_wait(5)
        self.clickemailfield()
        sleep(5)
        self.TypeEmail(email)
        #self.driver.hide_keyboard()
        self.driver.implicitly_wait(10)
        self.clickSignInButton()

    def verifyFailedLogin(self):
        self.driver.implicitly_wait(10)
        result = self.isElementPresent(self._forgot_password, locatorType="id")
        return result

    def FailedLoginWIthoutEmail(self,password):
        self.driver.implicitly_wait(10)
        self.clickSkipButton()
        self.driver.implicitly_wait(5)
        self.TypePassword(password)
        #self.driver.hide_keyboard()
        self.driver.implicitly_wait(10)
        self.clickSignInButton()

    def verifyFailedLoginWIthoutEmail(self):
        self.driver.implicitly_wait(10)
        result = self.isElementPresent(self._forgot_password, locatorType="id")
        return result

    def FailedLoginIncorrectCredentials(self,email,password):

        self.driver.implicitly_wait(10)
        self.clickSkipButton()
        self.driver.implicitly_wait(10)
        self.TypeEmail(email)
        #self.driver.hide_keyboard()
        self.driver.implicitly_wait(5)
        self.TypePassword(password)
        #self.driver.hide_keyboard()
        self.driver.implicitly_wait(10)
        self.clickSignInButton()

    def verifyFailedLoginWrongCredentials(self):
        self.driver.implicitly_wait(10)
        result = self.isElementPresent(self._forgot_password, locatorType="id")
        return result

    def FailedLoginKeepFieldsBlank(self):
        self.driver.implicitly_wait(10)
        self.clickSkipButton()
        self.driver.implicitly_wait(5)
        self.clickSignInButton()

    def verifyFailedLoginBlankCredentials(self):
        self.driver.implicitly_wait(10)
        result = self.isElementPresent(self._forgot_password, locatorType="id")
        return result

    def loginSuccess(self,email,password):
        self.driver.implicitly_wait(10)
        self.clickSkipButton()
        self.driver.implicitly_wait(5)
        self.TypeEmail(email)
        #self.driver.hide_keyboard()
        self.driver.implicitly_wait(5)
        self.TypePassword(password)
        #self.driver.hide_keyboard()
        self.driver.implicitly_wait(10)
        self.clickSignInButton()

    def verifyLoginSuccess(self):
        self.driver.implicitly_wait(10)
        result = self.isElementPresent(self._search_icon, locatorType="id")
        return result