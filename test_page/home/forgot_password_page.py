from base.selenium_driver import SeleniumDriver
import utilities.custom_logger as cl
from time import sleep
from appium.webdriver.common.touch_action import TouchAction
import logging
from selenium.webdriver.support.ui import WebDriverWait

import pytest

class ForgotPasswordPage(SeleniumDriver):
    log = cl.customlogger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    #locators
    _skip_button="com.dgmarket.android:id/tv_skip"#id
    _forgot_password="com.dgmarket.android:id/tv_forgot_password"#id
    _forgot_password_title="com.dgmarket.android:id/tv_todo_title"#id
    _email_forgot="com.dgmarket.android:id/et_email_forgot"#id
    _reset_passowrd_button="com.dgmarket.android:id/btn_recover_password"#id

    def clickSkipButton(self):
        self.elementClick(self._skip_button, locatorType="id")
    def ClickForgotPassword(self):
        self.elementClick(self._forgot_password,locatorType="id")
    def TypeEmail(self,email):
        self.sendKeys(email,self._email_forgot)
    def ForgetPassInitialize(self):
        self.elementClick(self._reset_passowrd_button,locatorType="id")


    def ForgetPasswordform(self):
        self.driver.implicitly_wait(10)
        self.clickSkipButton()
        self.driver.implicitly_wait(7)
        self.ClickForgotPassword()

    def verifyFOrgotPassFormShown(self):
        self.driver.implicitly_wait(10)
        result = self.isElementPresent(self._forgot_password_title, locatorType="id")
        return result

    def ForgetPassInitiate(self,email):
        self.driver.implicitly_wait(10)
        self.clickSkipButton()
        self.driver.implicitly_wait(7)
        self.ClickForgotPassword()
        self.driver.implicitly_wait(7)
        self.TypeEmail(email)
        # self.driver.hide_keyboard()
        self.driver.implicitly_wait(7)
        self.ForgetPassInitialize()

    def verifyForgetPass(self):
        self.driver.implicitly_wait(10)
        result = self.isElementPresent(self._forgot_password, locatorType="id")
        return result

    def InvalidEmail(self,email):
        self.driver.implicitly_wait(10)
        self.clickSkipButton()
        self.driver.implicitly_wait(7)
        self.ClickForgotPassword()
        self.driver.implicitly_wait(7)
        self.TypeEmail(email)
        self.driver.implicitly_wait(7)
        # self.driver.hide_keyboard()
        # self.driver.implicitly_wait(7)
        self.ForgetPassInitialize()

    def verifyInvalidEmail(self):
        self.driver.implicitly_wait(10)
        result = self.isElementPresent(self._forgot_password, locatorType="id")
        return result

    def TryWithEmptyFields(self):
        self.driver.implicitly_wait(10)
        self.clickSkipButton()
        self.driver.implicitly_wait(7)
        self.ClickForgotPassword()
        self.driver.implicitly_wait(7)
        self.ForgetPassInitialize()

    def VerifyEmptyFieldTry(self):
        self.driver.implicitly_wait(10)
        result = self.isElementPresent(self._forgot_password, locatorType="id")
        return result