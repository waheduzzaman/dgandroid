from base.selenium_driver import SeleniumDriver
import utilities.custom_logger as cl
from time import sleep
from appium.webdriver.common.touch_action import TouchAction
import logging
from selenium.webdriver.support.ui import WebDriverWait

import pytest

class GenericPages(SeleniumDriver):
    log = cl.customlogger(logging.DEBUG)

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    #locators
    _menu_button="//android.view.ViewGroup[@index=0]/android.widget.ImageButton[@index=0]"#xpath
    _about_us="//android.support.v7.widget.ar[@index=8]"
    _about_us_result="//android.view.View[@text='About dgMarket']"
    _rate_us="//android.support.v7.widget.ar[@index=9]"
    _rate_us_result="com.android.vending:id/right_button"#id
    _terms="//android.support.v7.widget.ar[@index=10]"
    _terms_result="//android.view.View[@text='dgMarket - Terms and Conditions']"
    _privacy_policy="//android.support.v7.widget.ar[@index=11]"
    _privacy_policy_result="//android.view.View[@text='Privacy Policy']"
    _feedback="//android.support.v7.widget.ar[@index=6]"
    _feedback_subject="com.dgmarket.android:id/et_subject"#id
    _feedback_message="com.dgmarket.android:id/et_msg"#id
    _submit_button="com.dgmarket.android:id/btn_submit"#id
    _failed_text1="//android.widget.TextView[@text='Missing the subject']"
    _failed_text2="//android.widget.TextView[@text='You have not wrote anything in the message']"
    _ContactUs="//android.support.v7.widget.ar[@index=7]"
    _contactUs_result="com.dgmarket.android:id/btn_call_us"#id
    _subscription="//android.support.v7.widget.ar[@index=1]"
    _subscription_result="com.dgmarket.android:id/tv_name"#id
    _edit_profile="//android.support.v7.widget.ar[@index=3]"
    _first_name="com.dgmarket.android:id/et_first_name"#id
    _last_name="com.dgmarket.android:id/et_last_name"#id
    _org_name="com.dgmarket.android:id/et_organization_name"#id
    _phone_number="com.dgmarket.android:id/et_phone"#id
    _url="com.dgmarket.android:id/et_website_url"#id
    _submit_button1="com.dgmarket.android:id/btn_submit"#id
    _editProfileNoFirstName="//android.widget.TextView[@text='First Name is required']"
    _editProfileNoLastName="//android.widget.TextView[@text='Last Name is required']"
    _editProfileNoOrgName="//android.widget.TextView[@text='Organization Name is required']"
    _editProfileNoPhone="//android.widget.TextView[@text='Phone Number is required']"
    _change_password="//android.support.v7.widget.ar[@index=4]"
    _password="com.dgmarket.android:id/et_password"#id
    _confirm_password="com.dgmarket.android:id/et_confirm_password"#id
    _reset_password_button="com.dgmarket.android:id/btn_recover_password"#id

    #methods
    def ClicMenuButton(self):
        self.elementClick(self._menu_button,locatorType='xpath')
    def ClickAboutUs(self):
        self.elementClick(self._about_us,locatorType='xpath')
    def ClickRateUs(self):
        self.elementClick(self._rate_us,locatorType='xpath')
    def ClickTerms(self):
        self.elementClick(self._terms,locatorType='xpath')
    def ClickPrivacyPOlicy(self):
        self.elementClick(self._privacy_policy,locatorType='xpath')
    def ClickFeedback(self):
        self.elementClick(self._feedback,locatorType='xpath')
    def TypeSubject(self,subject):
        self.sendKeys(subject,self._feedback_subject)
    def TypeMessage(self,message):
        self.sendKeys(message,self._feedback_message)
    def ClickSubmitButton(self):
        self.elementClick(self._submit_button,locatorType='id')
    def ClickContactUs(self):
        self.elementClick(self._ContactUs,locatorType="xpath")
    def ClickSubscription(self):
        self.elementClick(self._subscription,locatorType="xpath")
    def ClickEditProfile(self):
        self.elementClick(self._edit_profile,locatorType="xpath")
    def ClickFirstName(self):
        self.elementClick(self._first_name,locatorType="id")
    def TypeFirstName(self,firstname):
        self.sendKeys(firstname,self._first_name)
    def ClickLastName(self):
        self.elementClick(self._last_name,locatorType="id")
    def TypeLastName(self,lastname):
        self.sendKeys(lastname,self._last_name)
    def ClickOrgName(self):
        self.elementClick(self._org_name,locatorType='id')
    def TypeOrgName(self,orgname):
        self.sendKeys(orgname,self._org_name)
    def ClickPhoneNumber(self):
        self.elementClick(self._phone_number,locatorType='id')
    def TypePhoneNumber(self,phone):
        self.sendKeys(phone,self._phone_number)
    def CLickUrl(self):
        self.elementClick(self._url,locatorType='id')
    def TypeUrl(self,url):
        self.sendKeys(url,self._url)
    def ClickSubmitButton1(self):
        self.elementClick(self._submit_button1,locatorType="id")
    def ClickChangePassword(self):
        self.elementClick(self._change_password,locatorType="xpath")
    def TypePassword(self,password):
        self.sendKeys(password,self._password)
    def TypeConfirmPass(self,confirm):
        self.sendKeys(confirm,self._confirm_password)
    def ClickResetPasswordButton(self):
        self.elementClick(self._reset_password_button,locatorType='id')




    #classes
    def SendFeedbackTrue(self,subject,message):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickFeedback()
        self.driver.implicitly_wait(10)
        self.TypeSubject(subject)
        self.driver.implicitly_wait(10)
        self.TypeMessage(message)
        self.driver.implicitly_wait(10)
        self.ClickSubmitButton()

    def verifyFeedbackSubmission(self):
        sleep(5)
        result=self.isElementPresent(self._submit_button,locatorType='id')
        return result

    def SendFeedbackWithoutSubject(self,message):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickFeedback()
        self.driver.implicitly_wait(10)
        self.TypeMessage(message)
        self.driver.implicitly_wait(10)
        self.ClickSubmitButton()

    def VerifyFeebackWithoutSub(self):
        self.driver.implicitly_wait(10)
        result=self.isElementPresent(self._failed_text1,locatorType='xpath')
        return result

    def SendFeedbackWIthoutmsg(self,subject):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickFeedback()
        self.driver.implicitly_wait(10)
        self.TypeSubject(subject)
        self.driver.implicitly_wait(10)
        self.ClickSubmitButton()

    def VerifyFeedbackWithoutMsg(self):
        self.driver.implicitly_wait(10)
        result=self.isElementPresent(self._failed_text2,locatorType='xpath')
        return result

    def AboutUs(self):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickAboutUs()

    def VerifyAboutUs(self):
        sleep(5)
        result=self.isElementPresent(self._about_us_result,locatorType='xpath')
        return result

    def RateUs(self):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickRateUs()

    def VerifyRateUs(self):
        sleep(5)
        result=self.isElementPresent(self._rate_us_result,locatorType='id')
        return result

    def TermsConditions(self):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickTerms()

    def VerifyTerms(self):
        sleep(5)
        result=self.isElementPresent(self._terms_result,locatorType='xpath')
        return result

    def PrivacyPolicy(self):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickPrivacyPOlicy()

    def VerifyPrivacyPolicy(self):
        sleep(5)
        result=self.isElementPresent(self._privacy_policy_result,locatorType='xpath')
        return result

    def ContactUs(self):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickContactUs()

    def VerifyContactUs(self):
        sleep(5)
        result=self.isElementPresent(self._contactUs_result,locatorType="id")
        return result

    def Subscription(self):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickSubscription()

    def VerifySubscription(self):
        sleep(5)
        result=self.isElementPresent(self._subscription_result,locatorType="id")
        return result

    def EditProfile(self,firstname,lastname,orgname,phone,url):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickEditProfile()
        self.driver.implicitly_wait(10)
        self.TypeFirstName(firstname)
        self.driver.implicitly_wait(10)
        self.TypeLastName(lastname)
        self.driver.implicitly_wait(10)
        self.TypeOrgName(orgname)
        self.driver.implicitly_wait(10)
        self.TypePhoneNumber(phone)
        self.driver.implicitly_wait(10)
        self.TypeUrl(url)
        self.driver.implicitly_wait(10)
        self.ClickSubmitButton1()

    def verifyEditProfile(self):
        sleep(5)
        result=self.isElementPresent(self._subscription,locatorType='xpath')
        return result

    def EditProfileFirstNameBlank(self,firstname):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickEditProfile()
        self.driver.implicitly_wait(10)
        self.TypeFirstName(firstname)
        self.driver.press_keycode(67)
        self.driver.implicitly_wait(10)
        self.ClickSubmitButton1()

    def VerifyEditProfileNoFirstname(self):
        self.driver.implicitly_wait(10)
        result=self.isElementPresent(self._editProfileNoFirstName,locatorType='xpath')
        return result


    def EditProfileNoLastName(self,lastname):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickEditProfile()
        self.driver.implicitly_wait(10)
        self.TypeLastName(lastname)
        self.driver.press_keycode(67)
        self.driver.implicitly_wait(10)
        self.ClickSubmitButton1()

    def VerifyEditProfileNoLastName(self):
        self.driver.implicitly_wait(10)
        result=self.isElementPresent(self._editProfileNoLastName,locatorType='xpath')
        return result

    def EditProfileNoOrgName(self,orgname):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickEditProfile()
        self.driver.implicitly_wait(10)
        self.TypeOrgName(orgname)
        self.driver.press_keycode(67)
        self.driver.implicitly_wait(10)
        self.ClickSubmitButton1()

    def VerifyEditProfileNoOrgName(self):
        self.driver.implicitly_wait(10)
        result=self.isElementPresent(self._editProfileNoOrgName,locatorType='xpath')
        return result

    def EditProfileNoPhone(self,phone):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickEditProfile()
        self.driver.implicitly_wait(10)
        self.TypePhoneNumber(phone)
        self.driver.press_keycode(67)
        self.driver.implicitly_wait(10)
        self.ClickSubmitButton1()

    def VerifyNoPhone(self):
        self.driver.implicitly_wait(10)
        result = self.isElementPresent(self._editProfileNoPhone, locatorType='xpath')
        return result

    def ResetPassword(self,password,confirm):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickChangePassword()
        self.driver.implicitly_wait(10)
        self.TypePassword(password)
        self.driver.implicitly_wait(10)
        self.TypeConfirmPass(confirm)
        self.driver.implicitly_wait(10)
        self.ClickResetPasswordButton()

    def VerifyPasswordReset(self):
        sleep(5)
        result=self.isElementPresent(self._subscription,locatorType='xpath')
        return result

    def ResetPasswordNoPass(self,confirm):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickChangePassword()
        self.driver.implicitly_wait(10)
        self.TypeConfirmPass(confirm)
        self.driver.implicitly_wait(10)
        self.ClickResetPasswordButton()

    def VerifyResetPasswordNoPass(self):
        sleep(5)
        result=self.isElementPresent(self._reset_password_button,locatorType='id')
        return result

    def ResetPasswordNoCOnfirm(self,password):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickChangePassword()
        self.driver.implicitly_wait(10)
        self.TypePassword(password)
        self.driver.implicitly_wait(10)
        self.ClickResetPasswordButton()

    def VerifyResetPasswordNoCOnfirm(self):
        sleep(5)
        result = self.isElementPresent(self._reset_password_button, locatorType='id')
        return result


    def ResetPasswordEmpty(self):
        self.driver.implicitly_wait(10)
        self.ClicMenuButton()
        self.driver.implicitly_wait(10)
        self.ClickChangePassword()
        self.driver.implicitly_wait(10)
        self.ClickResetPasswordButton()

    def VerifyResetPasswordEmpty(self):
        sleep(5)
        result = self.isElementPresent(self._reset_password_button, locatorType='id')
        return result
    # def EditProfileNoUrl(self):
    #     self.driver.implicitly_wait(10)
    #     self.ClicMenuButton()
    #     self.driver.implicitly_wait(10)
    #     self.ClickEditProfile()
    #     self.driver.implicitly_wait(10)
    #     self.CLickUrl()
    #     self.driver.press_keycode(67)
    #     self.driver.implicitly_wait(10)
    #     self.ClickSubmitButton1()
    #
    # def VerifyNoUrl(self):
    #     self.driver.implicitly_wait(10)
    #     result = self.isElementPresent(self._editProfileNoPhone, locatorType='xpath')
    #     return result


