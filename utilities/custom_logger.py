import inspect
import logging


def customlogger(logLevel=logging.DEBUG):

    loggerName= inspect.stack()[1][3]
    logger=logging.getLogger(loggerName)
    logger.setLevel(logging.DEBUG)

    fileHandler= logging.FileHandler("automation.log",mode='w') #a means append, if u want to overwrite then use w. using w will mean a new file created everytime
    fileHandler.setLevel(logLevel)

    formatter= logging.Formatter('%(asctime)s -%(name)s %(levelname)s: %(message)s',datefmt='%m/%d/%Y %I:%M:%S %p')
    fileHandler.setFormatter(formatter)
    logger.addHandler(fileHandler)

    return logger